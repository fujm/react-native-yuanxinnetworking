import {
    NativeModules,
    Platform
} from 'react-native';

const _module = NativeModules.YuanXinNetworkingManager;

export default class NetworkingManager {

    static putFile(parameters: Object): Promise {
        return _module.putFile(parameters);
    }

    static uploadFiles(parameters: Object): Promise {
        return _module.uploadFiles(parameters);
    }

    static removeImageCacheForUrl(url: String|Array<String>, size: Object): void {
        if (Platform.OS === 'ios') {
            _module.removeImageCacheForUrl(url, size ? size : {width: 0, height: 0});
        }
    }

    static clearAllImageCache(): void {
        if (Platform.OS === 'ios') {
            _module.clearAllImageCache();
        }
    }
}