//
//  NetworkingManager.h
//  YuanXinNetWorking
//
//  Created by 晏德智 on 2016/12/7.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFOAuthCredential.h"
#import "AFHTTPSessionManager.h"

@protocol OAuthInfoObserver <NSObject>

//网络请求401时处理
- (void)onUnauthorized;

@end


@interface NetworkingManager : NSObject

@property (readwrite, nonatomic, strong) NSURL    * baseURL;
@property (readwrite, nonatomic, strong) NSString * clientID;
@property (readwrite, nonatomic, strong) NSString * clientSecret;

/**
 * 默认超时时间
 */
@property (readwrite, assign, nonatomic) NSTimeInterval timeoutInterval;

/**
 * 是否处理 RN <Image/>组件 http https 路径的网络图片的加载
 */
@property (readwrite, assign, nonatomic) BOOL canLoadWebImageURL;
@property (readwrite, nonatomic, copy) NSString *bucketNameForIMUploadFile;

//添加对OAuth 服务状态管理
- (void)addOAuthInfoObserver:(id<OAuthInfoObserver>)oauthObserver;

//移除当前OAuth 服务状态管理
- (void)removeOAuthInfoObserver:(id<OAuthInfoObserver>)oauthObserver;

+ (instancetype)shareInstance;

//还原网络请求的身份信息
- (BOOL)retrieveCredentialWithIdentifier:(NSString *)identifier;

- (NSURLSessionDataTask *)GET:(NSString *)URLString
                   parameters:(id)parameters
                      success:(void(^)(NSURLSessionDataTask *task, id responseObject))success
                      failure:(void(^)(NSURLSessionDataTask * task, NSError * error))failure;

- (NSURLSessionDataTask *)POST:(NSString *)URLString
                    parameters:(id)parameters
                       success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                       failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)PUT:(NSString *)URLString
                   parameters:(id)parameters
                      success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)PATCH:(NSString *)URLString
                     parameters:(id)parameters
                        success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                        failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)DELETE:(NSString *)URLString
                      parameters:(id)parameters
                         success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                         failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionTask *)authenticateUsingOAuthWithURLString:(NSString *)URLString
                                                 username:(NSString *)username
                                                 password:(NSString *)password
                                                    scope:(NSString *)scope
                                                  success:(void (^)(AFOAuthCredential *credential))success
                                                  failure:(void (^)(NSError *error))failure;

- (NSURLSessionTask *)authenticateUsingOAuthWithURLString:(NSString *)URLString
                                               parameters:(NSDictionary *)parameters
                                                  success:(void (^)(id responseObject))success
                                                  failure:(void (^)(NSError *error))failure;

- (NSURLSessionDataTask *)uploadFile:(NSString *)URLString
                                data:(NSData *)data
                             headers:(NSDictionary *)headers
                             success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                             failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


- (NSURLSessionDataTask *)downloadImageFile:(NSString *)URLString
                                   cacheURL:(NSURL *)cacheUrl
                           downloadProgress:(void (^)(NSProgress *downloadProgress))downloadProgressBlock
                                    success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                    failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)downloadFile:(NSString *)URLString
                          headerAccept:(NSString *)headerAccept
                              cacheURL:(NSURL *)cacheUrl
                      downloadProgress:(void (^)(NSProgress *downloadProgress))downloadProgressBlock
                               success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                               failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)YuanXinDownloadImageFile:(NSString *)URLString
                                        parameters:(id)parameters
                                  downloadProgress:(void (^)(NSProgress *downloadProgress))downloadProgressBlock
                                           success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                           failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


@end
