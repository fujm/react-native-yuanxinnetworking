package com.yuanxin.networking;

import java.io.InputStream;

/**
 * Created by lemon on 2016/12/21.
 */

public interface DownFileCallBack {
    public void success(InputStream inputStream);
    public void error(Exception ex);
}
