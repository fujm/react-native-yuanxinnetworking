//
//  IMNetwoingFileTransfer.h
//  YuanXinNetWorking
//
//  Created by 晏德智 on 2016/12/15.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FileTransferProxyDelegate.h"

@interface IMNetwoingFileTransfer : NSObject<IMFileTransferDelegate>

@property (nonatomic, weak) id<IMFileTransferCallbackDelegate> delegate;

+ (instancetype)shareInstance;

@end
