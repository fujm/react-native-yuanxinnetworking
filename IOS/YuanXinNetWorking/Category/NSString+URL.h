//
//  NSString+URL.h
//  YuanXinNetWorking
//
//  Created by 晏德智 on 2016/12/7.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (URL)

/**
 * 去除首尾的空格
 */
- (NSString *)trim;

- (BOOL) isUrl;

@end
