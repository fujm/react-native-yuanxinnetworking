//
//  NSData+JsonObject.m
//  YuanXinKit
//
//  Created by GJS on 2017/6/22.
//  Copyright © 2017年 GJS. All rights reserved.
//

#import "NSData+JsonObject.h"

@implementation NSData (JsonObject)

/**
 * 将JSON串转化为字典或者数组
 */
- (id)jsonObject {
    
    NSError *error = nil;
    
    return [self jsonObjectError:error];
}

- (id)jsonObjectError:(NSError *)error {
    
    id jsonObject = [NSJSONSerialization JSONObjectWithData:self
                                                    options:NSJSONReadingAllowFragments
                                                      error:&error];
    
    if (jsonObject != nil && error == nil) {
        return jsonObject;
    } else {
        // 解析错误
        return nil;
    }
}

@end
