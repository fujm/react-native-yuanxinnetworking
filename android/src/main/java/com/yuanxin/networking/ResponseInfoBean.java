package com.yuanxin.networking;

/**
 * Created by lemon on 2016/12/20.
 */

public class ResponseInfoBean {
    private int code;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;
}
