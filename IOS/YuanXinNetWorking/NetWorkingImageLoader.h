//
//  NetWorkingPhotoLibraryImageLoader.h
//  YuanXinNetWorking
//
//  Created by 晏德智 on 2016/12/13.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#if __has_include(<React/RCTBridgeModule.h>)
#import <React/RCTImageLoader.h>
#else
#import <React/RCTImageLoader.h>
#endif

@interface NetWorkingImageLoader : NSObject<RCTImageURLLoader>

@end
