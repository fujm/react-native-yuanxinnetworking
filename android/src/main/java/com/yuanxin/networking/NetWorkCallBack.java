package com.yuanxin.networking;

/**
 * Created by lemon on 2016/12/19.
 */

public interface NetWorkCallBack {
    public void success(ResponseInfoBean responseInfoBean);
    public void error(Exception ex);
}
