//
//  AFHTTPRequestSerializer+OAuth.m
//  YuanXinNetWorking
//
//  Created by 晏德智 on 2016/12/7.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#import "AFHTTPRequestSerializer+OAuth.h"
#import "AFOAuthManager.h"
#import "AFOAuthCredential.h"

@implementation AFHTTPRequestSerializer (OAuth)

- (void)setAuthorizationHeaderFieldWithCredential:(AFOAuthCredential *)credential {
    if ([credential.tokenType compare:@"Bearer" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [self setValue:[NSString stringWithFormat:@"Bearer %@", credential.accessToken] forHTTPHeaderField:@"Authorization"];
    }
}

@end
