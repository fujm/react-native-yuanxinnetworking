//
//  NSString+URL.m
//  YuanXinNetWorking
//
//  Created by 晏德智 on 2016/12/7.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#import "NSString+URL.h"

@implementation NSString (URL)

- (NSString *)trim {
    
    if(self){
        NSString *cleanString = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        return cleanString;
    }else{
        return nil;
    }
}

- (BOOL)isUrl {
    BOOL result = false;
    if(self){
        NSString *cleanString = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSRange bankRang = NSMakeRange(0, 7);
        //todo:ydz 因为生产环境还不是https
        result = [[cleanString substringWithRange:bankRang] caseInsensitiveCompare:@"http://"];
        if(!result){
            bankRang.length = 8;
            result = [[cleanString substringWithRange:bankRang] caseInsensitiveCompare:@"https://"];
        }
    }
    
    return result;
}

@end
