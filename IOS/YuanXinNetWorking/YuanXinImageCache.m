//
//  YuanXinImageCache.m
//  YuanXinNetWorking
//
//  Created by GJS on 2018/2/5.
//  Copyright © 2018年 GJS. All rights reserved.
//

#import "YuanXinImageCache.h"
#import <SDWebImage/SDWebImage.h>
#if __has_include(<React/RCTBridge.h>)
#import <React/RCTUtils.h>
#else
#import <React/RCTUtils.h>
#endif

@implementation YuanXinImageCache

+ (UIImage *)loadImageFromSDImageCacheForKey:(NSString *)cacheKey {
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    UIImage *cacheImage = [imageCache imageFromMemoryCacheForKey:cacheKey];
    if (!cacheImage) {
        cacheImage = [imageCache imageFromDiskCacheForKey:cacheKey];
    }
    return cacheImage;
}

+ (void)storeImage:(UIImage *)image bySDImageCacheForKey:(NSString *)cacheKey {
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache storeImage:image forKey:cacheKey completion:nil];
}

+ (void)removeSDImageCacheForKey:(NSString *)cacheKey {
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache removeImageForKey:cacheKey withCompletion:nil];
}

+ (void)removeSDImageCacheForUrl:(NSString *)urlStr {
    [self removeSDImageCacheForUrl:urlStr alsoWithSize:CGSizeZero];
}

+ (void)removeSDImageCacheForUrl:(NSString *)urlStr alsoWithSize:(CGSize)size {
    [self removeSDImageCacheForKey:urlStr];
    [self removeSDImageCacheForUrl:urlStr withSize:CGSizeZero];
    if (!CGSizeEqualToSize(size, CGSizeZero)) {
        [self removeSDImageCacheForUrl:urlStr withSize:size];
    }
}

+ (void)removeSDImageCacheForUrl:(NSString *)urlStr withSize:(CGSize)size {
    // 其他size的缓存则只能有外面传入了，这里不可能会拿到
    NSString *parment = [NSString stringWithFormat:@"%@,w_%.2f,h_%.2f",urlStr,size.width,size.height];
    [self removeSDImageCacheForKey:parment];
}

+ (void)clearSDImageCacheDisk {
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache clearDiskOnCompletion:nil];
}

@end
