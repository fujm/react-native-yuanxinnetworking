//
//  RCTConvert+Networking.m
//  YuanXinNetWorking
//
//  Created by 晏德智 on 2016/12/8.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#import "RCTConvert+Networking.h"
#import "RCTConvert+FileUploadInfo.h"

@implementation RCTConvert (Networking)

RCT_ARRAY_CONVERTER(YuanXinUploadFiles)

+ (YuanXinUploadFiles *)YuanXinUploadFiles:(id)json {
    NSDictionary *fileData = [self NSDictionary:json];
    if(fileData){
        YuanXinUploadFiles *model = [[YuanXinUploadFiles alloc] init];
        model.bucketName = [self NSString:fileData[@"bucket"]]?:@"Office";
        model.resourceID = [self NSString:fileData[@"resourceID"]]?:[[[NSUUID UUID] UUIDString] stringByReplacingOccurrencesOfString:@"-" withString:@""];
        model.urlString = [self NSString:fileData[@"urlString"]]?:@"FileUploadService/api/upload";
        model.storageType =[self NSNumber:fileData[@"storageType"]].integerValue?:ByTime;
        NSArray *files = [self NSDictionaryArray:fileData[@"files"]];
        if(files && files.count >0){
            NSMutableArray *upFiles = [[NSMutableArray alloc] init];
            for (NSDictionary *itemInfo in files) {
                
                FileUploadInfo *upInfo = [self FileUploadInfo:itemInfo];
                [upFiles addObject:upInfo];
            }
            model.files = upFiles;
        }
        return model;
    }else{
        return  nil;
    }
}

@end
