//
//  UploadFileParameters.h
//  YuanXinNetWorking
//
//  Created by 晏德智 on 2016/12/9.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FileUploadInfo.h"

typedef NS_ENUM(NSInteger, UpStorageType){
    ByTime = 0,
    byFileType = 1
};

@interface YuanXinUploadFiles : NSObject

/**
 * 存储的根目录
 */
@property (nonatomic, copy) NSString    *bucketName;

/**
 * 资源ID
 */
@property (nonatomic, copy) NSString    *resourceID;

/**
 * 请求后台存储接口
 */
@property (nonatomic, copy) NSString    *urlString;

/*
 * 存储类开
 */
@property (nonatomic, assign) UpStorageType storageType;

/**
 * 文件信息
 */
@property (nonatomic, strong) NSArray<FileUploadInfo *>   *files;

@end
