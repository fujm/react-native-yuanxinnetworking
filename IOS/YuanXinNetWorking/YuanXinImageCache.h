//
//  YuanXinImageCache.h
//  YuanXinNetWorking
//
//  Created by GJS on 2018/2/5.
//  Copyright © 2018年 GJS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface YuanXinImageCache : NSObject

+ (UIImage *)loadImageFromSDImageCacheForKey:(NSString *)cacheKey;
+ (void)storeImage:(UIImage *)image bySDImageCacheForKey:(NSString *)cacheKey;
+ (void)removeSDImageCacheForKey:(NSString *)cacheKey;
+ (void)removeSDImageCacheForUrl:(NSString *)urlStr;
+ (void)removeSDImageCacheForUrl:(NSString *)urlStr alsoWithSize:(CGSize)size;
+ (void)clearSDImageCacheDisk;

@end
