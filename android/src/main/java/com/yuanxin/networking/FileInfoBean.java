package com.yuanxin.networking;

import java.util.Arrays;

/**
 * Created by lemon on 2016/12/19.
 */

public class FileInfoBean {
    public FileInfoBean(String filePath, byte[] fileByte,String objectKey) {
        this.filePath = filePath;
        this.fileByte = fileByte;
        this.objectKey=objectKey;
        setFormatPath(filePath);
        setAudioPath(filePath);
        setVideoPath(filePath);
    }

    private String objectKey;

    public String getObjectKey() {
        return objectKey;
    }

    public void setObjectKey(String objectKey) {
        this.objectKey = objectKey;
    }

    private String filePath;
    private String formatPath;//完整路径图片
    private String audioPath;//完整语音路径
    private String videoPath;
    private byte[] fileByte;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
        setFormatPath(filePath);
        setAudioPath(filePath);
        setVideoPath(filePath);
    }

    public byte[] getFileByte() {
        return fileByte;
    }

    public void setFileByte(byte[] fileByte) {
        this.fileByte = fileByte;
    }

    public String getFormatPath() {
        return formatPath;
    }

    public void setFormatPath(String formatPath) {
        this.formatPath = formatPath+".jpg";
    }

    public String getAudioPath() {
        return audioPath;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath+".mp4";
    }

    public void setAudioPath(String audioPath) {
        this.audioPath = audioPath+".amr";
    }

    @Override
    public String toString() {
        return "FileInfoBean{" +
                "objectKey='" + objectKey + '\'' +
                ", filePath='" + filePath + '\'' +
                '}';
    }
}
