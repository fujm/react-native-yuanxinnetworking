//
//  AFHTTPRequestSerializer+OAuth.h
//  YuanXinNetWorking
//
//  Created by 晏德智 on 2016/12/7.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#import "AFURLRequestSerialization.h"
@class AFOAuthCredential;

@interface AFHTTPRequestSerializer (OAuth)

/**
 Sets the "Authorization" HTTP header set in request objects made by the HTTP client to contain the access token within the OAuth credential. This overwrites any existing value for this header.
 @param credential The OAuth2 credential
 */
- (void)setAuthorizationHeaderFieldWithCredential:(AFOAuthCredential *)credential;

@end

