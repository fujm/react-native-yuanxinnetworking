//
//  NSData+JsonObject.h
//  YuanXinKit
//
//  Created by GJS on 2017/6/22.
//  Copyright © 2017年 GJS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (JsonObject)

/**
 * 将JSON串转化为字典或者数组
 */
- (id)jsonObject;
- (id)jsonObjectError:(NSError *)error;

@end
